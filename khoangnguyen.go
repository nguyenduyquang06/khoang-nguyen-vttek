package main

import (
	"fmt"
	"math/rand"
	"time"
)

type KhoangNguyen struct {
	a         int
	b         int
	kichthuoc int
}

func main() {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	N := 7
	DayKhoangNguyen := make([]KhoangNguyen, N)
	count := 0
	//(-2,1) (-1,3) (2, 3) (4,5)
	// DayKhoangNguyen[0] = KhoangNguyen{-2, 10, 2}
	// DayKhoangNguyen[1] = KhoangNguyen{-1, 5, 5}
	// DayKhoangNguyen[2] = KhoangNguyen{2, 4, 1}
	// DayKhoangNguyen[3] = KhoangNguyen{4, 6, 1}
	// DayKhoangNguyen[4] = KhoangNguyen{5, 15, 9}
	//fmt.Println(checkRoiMang(DayKhoangNguyen[3], []KhoangNguyen{DayKhoangNguyen[2]}))
	for i := 0; i < N; i++ {
		//DayKhoangNguyen[i].a = rand.New(rand.NewSource(time.Now().UnixNano())).Int31n(1000) - 500
		// if float(count) < 0.3*float(i) {
		// 	DayKhoangNguyen[i].a = int(r.NormFloat64()*10 + 0)
		// 	for !(DayKhoangNguyen[i].a < -1) {
		// 		DayKhoangNguyen[i].a = int(r.NormFloat64()*10 + 0)
		// 	}
		// }
		DayKhoangNguyen[i].a = int(r.NormFloat64()*10 + 0)
		DayKhoangNguyen[i].kichthuoc = int(rand.New(rand.NewSource(time.Now().UnixNano()*int64(i))).Int31n(10)) + 1
		DayKhoangNguyen[i].b = DayKhoangNguyen[i].a + DayKhoangNguyen[i].kichthuoc + 1
		if DayKhoangNguyen[i].a < -1 {
			count++
		}
	}
	//fmt.Println(float32(count) / float32(N))
	//fmt.Printf("(%d %d) Size: %d \n", DayKhoangNguyen[0].a, DayKhoangNguyen[0].b, DayKhoangNguyen[0].kichthuoc)
	fmt.Println("Cac khoang nguyen:")
	for i := 0; i < N; i++ {
		fmt.Printf("(%d %d) Size: %d \n", DayKhoangNguyen[i].a, DayKhoangNguyen[i].b, DayKhoangNguyen[i].kichthuoc)
	}
	ex := TimKNToiDa(DayKhoangNguyen)
	fmt.Println("Khoang nguyen toi da:", len(ex))
	for _, val := range ex {
		fmt.Printf("(%d %d)\n", val.a, val.b)
	}

}

// func TimKNToiDa(data_in []KhoangNguyen) []KhoangNguyen {
// 	max := -1
// 	pos := -1
// 	temp := make([][]KhoangNguyen, len(data_in))
// 	for i, val := range data_in {
// 		temp1 := append(data_in[:i], data_in[i+1:]...)
// 		if len(temp1) == 1 {
// 			return []KhoangNguyen{val}
// 		}
// 		temp[i] = append(TimKNToiDa(temp1), val)
// 		if len(temp[i]) > max {
// 			max = len(temp[i])
// 			pos = i
// 		}
// 	}
// 	return temp[pos]
// }

func TimKNToiDa(data_in []KhoangNguyen) []KhoangNguyen {
	max := 0
	pos := -1
	temp := make([][]KhoangNguyen, len(data_in))
	for i, val := range data_in {
		arr := make([]KhoangNguyen, len(data_in))
		copy(arr, data_in)
		arr = append(arr[:i], arr[i+1:]...)
		temp1 := make([]KhoangNguyen, len(arr))
		copy(temp1, arr)
		if len(temp1) == 0 {
			return []KhoangNguyen{val}
		}
		temp2 := TimKNToiDa(temp1)
		if checkRoiMang(val, temp2) {
			arr2 := append([]KhoangNguyen{val}, temp2...)
			temp[i] = make([]KhoangNguyen, len(arr2))
			copy(temp[i], arr2)
		} else {
			temp[i] = temp2
		}
		if len(temp[i]) > max {
			max = len(temp[i])
			pos = i
		}
		if len(data_in) == max {
			break
		}
	}
	return temp[pos]
}

func checkRoiMang(a KhoangNguyen, data_in []KhoangNguyen) bool {
	for _, val := range data_in {
		if !checkRoi(val, a) {
			return false
		}
	}
	return true
}

func checkRoi(a, b KhoangNguyen) bool {
	for i := a.a + 1; i < a.b; i++ {
		for j := b.a + 1; j < b.b; j++ {
			if i == j {
				return false
			}
		}
	}
	return true
}

func checkThuoc(val int, a KhoangNguyen) bool {
	if val < a.b && val > a.a {
		return true
	}
	return false
}
